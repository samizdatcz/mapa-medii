var drawNet = function() {
    d3.json("./media/data.json", function(data) {

        //min & max size
        var allSizes = []
        data['nodes'].reduce(function(prevVal, elem) {
            allSizes.push(elem.size);
        }, 0);

        var min = Math.min.apply(null, allSizes)
        var max = Math.max.apply(null, allSizes)

        // scales
        var colorScale = d3.scale.linear()
            .domain([min, min + (max - min) / 2, min + (max - min), max])
            .range(['#eff3ff', '#bdd7e7', '#3182bd', '#08519c']);

        var sizeScale = d3.scale.linear()
            .domain([Math.min.apply(null, allSizes), Math.max.apply(null, allSizes)])
            .range([10, 100]);

        // network
        var net = new sigma('net');

        for (var node in data['nodes']) {
          net.graph.addNode({
            id: data['nodes'][node]['id'],
            label: data['nodes'][node]['id'],
            x: data['nodes'][node]['x'],
            y: data['nodes'][node]['y'],
            size: sizeScale(data['nodes'][node]['size']),
            color: colorScale(data['nodes'][node]['size'])
          })
        }

        for (var edge in data['edges']) {
          net.graph.addEdge({
            id: data['edges'][edge]['id'],
            source: data['edges'][edge]['source'],
            target: data['edges'][edge]['target']
          })
        }

        net.settings({
            labelThreshold: 5,
            borderSize: 1,
            defaultNodeBorderColor: '#fc9272',
            fontStyle: "font-family: 'Roboto', sans-serif;",
            defaultLabelSize: 12,
            nodeHoverColor: 'default'
        })

        net.refresh();
    });
}

drawNet();