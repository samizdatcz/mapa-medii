// Pokud přejmenováváš, měň vždy klíč, nikoli hodnotu (to první, ne to druhý)!
var names = {
	'DVTV': 'DVTV.cz',
	'Respekt': 'tydenikrespekt',
	'iDNES.cz': 'iDNES.cz',
	'Reflex': 'reflexcz',
	'Reportér': 'reportermagazin',
	'Hosp. noviny': 'ihned.cz',
	'ČT24': 'CT24.cz',
	'Aktuálně.cz': 'Aktualne.cz',
	'Blesk.cz': 'Blesk.cz',
	'Novinky.cz': 'Novinky.cz',
	'AC24.cz': 'AC24.cz',
	'TN.cz': 'tn.nova',
	'Parlamentní listy': 'parlamentnilisty.cz',
	'Týdeník Echo': 'denikecho24',
	'A2larm': '623521487687579',
	'Radiožurnál': 'radiozurnal',
	'Týdeník Policie': 'tydenikpolicie',
	'Svobodné fórum': '727173144044824',
	'Neovlivní.cz': 'neovlivnicz',
	'EuroZprávy.cz': 'EuroZpravy.cz',
	'Týden': 'tyden',
	'Deník E15': 'denikE15',
	'D. Referendum': 'DReferendum',
	'lidovky.cz': 'lidovky.cz',
	'Sputnik': 'cz.sputnik',
	'Rozhlas Zprávy': 'zpravy.rozhlas.cz',
	'Aeronet': 'aeronet.cz',
};

var matrix = [-1, 280, 125, 124, 183, 216, 178, 223, 6, 78, 21, 7, 32, 146, 80, 117, 12, 110, 146, 28, 53, 48, 45, 64, 2, 33, 3
			,280, -1, 108, 156, 236, 285, 162, 244, 12, 52, 14, 17, 28, 194, 149, 91, 8, 114, 168, 25, 80, 41, 89, 78, 2, 48, 0
			,125, 108, -1, 211, 73, 190, 275, 188, 85, 186, 38, 142, 113, 120, 23, 69, 50, 71, 45, 109, 130, 63, 14, 59, 15, 36, 3
			,124, 156, 211, -1, 108, 203, 176, 146, 50, 103, 27, 57, 92, 178, 16, 95, 30, 145, 90, 90, 155, 55, 10, 41, 10, 31, 1
			,183, 236, 73, 108, -1, 167, 106, 138, 9, 36, 18, 11, 39, 183, 76, 81, 17, 130, 192, 29, 64, 34, 66, 63, 9, 40, 3
			,216, 285, 190, 203, 167, -1, 224, 254, 86, 203, 14, 20, 41, 165, 75, 101, 20, 87, 140, 53, 107, 90, 58, 80, 11, 45, 2
			,178, 162, 275, 176, 106, 224, -1, 190, 58, 122, 29, 88, 78, 125, 31, 102, 52, 104, 89, 93, 94, 56, 17, 45, 15, 51, 4
			,223, 244, 188, 146, 138, 254, 190, -1, 40, 138, 31, 52, 69, 153, 72, 115, 31, 107, 135, 69, 116, 73, 43, 91, 12, 59, 3
			,6, 12, 85, 50, 9, 86, 58, 40, -1, 149, 17, 128, 40, 25, 2, 12, 29, 18, 9, 98, 43, 18, 2, 12, 5, 5, 3
			,78, 52, 186, 103, 36, 203, 122, 138, 149, -1, 43, 81, 98, 80, 17, 55, 45, 51, 35, 90, 86, 53, 8, 36, 20, 29, 9
			,21, 14, 38, 27, 18, 14, 29, 31, 17, 43, -1, 18, 137, 40, 3, 10, 13, 15, 7, 62, 28, 28, 1, 6, 69, 4, 40
			,7, 17, 142, 57, 11, 20, 88, 52, 128, 81, 18, -1, 49, 22, 0, 12, 58, 14, 3, 110, 38, 26, 0, 6, 4, 8, 0
			,32, 28, 113, 92, 39, 41, 78, 69, 40, 98, 137, 49, -1, 100, 7, 28, 25, 53, 27, 191, 91, 54, 5, 17, 81, 4, 46
			,146, 194, 120, 178, 183, 165, 125, 153, 25, 80, 40, 22, 100, -1, 30, 74, 14, 238, 187, 77, 84, 68, 22, 41, 26, 31, 9
			,80, 149, 23, 16, 76, 75, 31, 72, 2, 17, 3, 0, 7, 30, -1, 27, 1, 27, 56, 2, 12, 7, 296, 27, 0, 10, 0
			,117, 91, 69, 95, 81, 101, 102, 115, 12, 55, 10, 12, 28, 74, 27, -1, 11, 48, 63, 25, 52, 33, 19, 34, 1, 70, 1
			,12, 8, 50, 30, 17, 20, 52, 31, 29, 45, 13, 58, 25, 14, 1, 11, -1, 13, 7, 25, 9, 19, 1, 6, 3, 11, 1
			,110, 114, 71, 145, 130, 87, 104, 107, 18, 51, 15, 14, 53, 238, 27, 48, 13, -1, 175, 54, 57, 35, 15, 27, 2, 18, 4
			,146, 168, 45, 90, 192, 140, 89, 135, 9, 35, 7, 3, 27, 187, 56, 63, 7, 175, -1, 24, 54, 38, 44, 51, 0, 41, 1
			,28, 25, 109, 90, 29, 53, 93, 69, 98, 90, 62, 110, 191, 77, 2, 25, 25, 54, 24, -1, 81, 45, 1, 17, 36, 12, 18
			,53, 80, 130, 155, 64, 107, 94, 116, 43, 86, 28, 38, 91, 84, 12, 52, 9, 57, 54, 81, -1, 42, 11, 46, 7, 25, 3
			,48, 41, 63, 55, 34, 90, 56, 73, 18, 53, 28, 26, 54, 68, 7, 33, 19, 35, 38, 45, 42, -1, 5, 22, 12, 18, 2
			,45, 89, 14, 10, 66, 58, 17, 43, 2, 8, 1, 0, 5, 22, 296, 19, 1, 15, 44, 1, 11, 5, -1, 20, 1, 11, 0
			,64, 78, 59, 41, 63, 80, 45, 91, 12, 36, 6, 6, 17, 41, 27, 34, 6, 27, 51, 17, 46, 22, 20, -1, 4, 24, 1
			,2, 2, 15, 10, 9, 11, 15, 12, 5, 20, 69, 4, 81, 26, 0, 1, 3, 2, 0, 36, 7, 12, 1, 4, -1, 2, 31
			,33, 48, 36, 31, 40, 45, 51, 59, 5, 29, 4, 8, 4, 31, 10, 70, 11, 18, 41, 12, 25, 18, 11, 24, 2, -1, 0
			,3, 0, 3, 1, 3, 2, 4, 3, 3, 9, 40, 0, 46, 9, 0, 1, 1, 4, 1, 18, 3, 2, 0, 1, 31, 0, -1
];

var makeTooltip = function (d, i) {
	part1 = Object.keys(names)[i % Object.keys(names).length]
	part2 = Object.keys(names)[Math.floor(i / Object.keys(names).length)]
	if (part1 != part2) {
		var tooltip = d3.select('#matrix').append('div')
			.html('Příspěvky ze stránek <b>' + part1 + '</b> a zároveň <b>' + part2 + '</b> lajkovalo v poslední době <b>' + d + ' uživatelů</b>.') //text tooltipu
			.style('left', (i % Object.keys(names).length) * (width / Object.keys(names).length) + padding + 30 + 'px')
			.style('top', Math.floor(i / Object.keys(names).length) * (height / Object.keys(names).length) + padding + 30 + 'px')
			.attr('class', 'tooltip')
	}
};

var padding = 95;

var width = 650;
var height = 650;

var svg = d3.select('#matrix').append('svg')
	.attr('width', width + padding)
	.attr('height', height + padding);

var scale = d3.scale.ordinal()
			.domain(Object.keys(names))
			.rangePoints([0, width - padding + 65])

var colorScale = d3.scale.linear()
		.domain([0, 74, 148,  296])
		.range(['#fee5d9', '#fcae91', '#fb6a4a', '#cb181d'])

var xAxis = d3.svg.axis()
		.scale(scale)
		.orient('bottom')

var yAxis = d3.svg.axis()
		.scale(scale)
		.orient('right')

var xaxisContent = svg.append('g')
		.attr('class', 'xaxis')
		.attr('transform', 'translate(' + (padding/2 + 47) + ', 46)')
		.call(xAxis)

svg.selectAll('g.xaxis text')
		.attr('transform', 'rotate(-90)')

var yaxisContent = svg.append('g')
		.attr('class', 'yaxis')
		.style('cursor', 'hand')
		.attr('transform', 'translate(' + (padding/2 - 47) + ', 110)')
		.call(yAxis)

var yTicks = d3.select('.yaxis')
				.selectAll('.tick')
				.style('text-decoration', 'underline')
				.on('click', function (e) {
					window.open('http://facebook.com/' + names[e])
				})

var table = svg.selectAll('g.value')
	.data(matrix)
	.enter()
	.append('g')
	.attr('class', 'cell')
	.on('mouseover', function (d, i) {
		icko = i
		svg.selectAll('rect')
			.filter(function (d, i) { 
				return ((i % Object.keys(names).length) == (icko % Object.keys(names).length) || Math.floor(i / Object.keys(names).length) == Math.floor(icko / Object.keys(names).length) ) 
			})
			.attr('class', 'hover')
		
		xaxisContent.selectAll('text')
			.filter(function(d, i) {
				return ((i % Object.keys(names).length) == (icko % Object.keys(names).length))
			})
			.attr('class', 'hoverBold')

		yaxisContent.selectAll('text')
			.filter(function(d, i) {
				return (i == Math.floor(icko / Object.keys(names).length))
			})
			.attr('class', 'hoverBold')

		makeTooltip(d, i)
	})
	.on('mouseout', function (d, i) {
		d3.select('.tooltip').remove()
		icko = i
		svg.selectAll('rect')
			.filter(function (d, i) {
				return ((i % Object.keys(names).length) == (icko % Object.keys(names).length) || Math.floor(i / Object.keys(names).length) == Math.floor(icko / Object.keys(names).length) ) 
			})
			.attr('class', '')
		xaxisContent.selectAll('text')
			.filter(function(d, i) {
				return ((i % Object.keys(names).length) == (icko % Object.keys(names).length))
			})
			.attr('class', 'xtick')		

		yaxisContent.selectAll('text')
			.filter(function(d, i) {
				return (i == Math.floor(icko / Object.keys(names).length))
			})
			.attr('class', 'ytick')		
	})

table.append('rect')
		.attr('x', function(d, i) {
			return (i % Object.keys(names).length) * (width / Object.keys(names).length) + padding
		})
		.attr('y', function(d, i) {
			return Math.floor(i / Object.keys(names).length) * (height / Object.keys(names).length) + padding
		})
		.attr('height', 20)
		.attr('width', 20)
		.attr('fill', function(d) {
			if (d == -1) {
				return 'lightgray';
			} else {
				return colorScale(d);
			}
		});

table.append('text')
	.attr('text-anchor', 'middle')
	.text(function (d, i) {
		if (d == -1) {
			return '';
		} else {
			return d;
		}
	})
	.attr('x', function(d, i) {
		return (i % Object.keys(names).length) * (width / Object.keys(names).length) + 10 + padding
	})
	.attr('y', function(d, i) {
		return Math.floor(i / Object.keys(names).length) * (height / Object.keys(names).length) + 14 + padding
	})
	.attr('font-size', '11px')
	.attr('fill', 'black');