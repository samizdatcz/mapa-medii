---
title: "Zpravodajství, bulvár i dezinformace: Mapa médií na sociálních sítích"
perex: "Jak blízko k sobě mají weby tradičních médií a nové internetové projekty? A co spojuje dezinformační weby se seriózním zpravodajstvím? Viděno pohledem sociálních sítí tvoří média několik oddělených ostrovů. Český rozhlas analyzoval facebookové stránky mediálních webů a sestavil unikátní mapu médií."
authors: ["Jan Cibulka", "Michal Zlatkovský"]
published: "xx. prosince 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "mapa-medii"
libraries: ["https://cdnjs.cloudflare.com/ajax/libs/sigma.js/1.2.0/sigma.min.js", d3, jquery]
styles: ["./media/matrix.css"]
---

Vybrali jsme přes třicet facebookových stránek seriózních i [dezinformačních](https://interaktivni.rozhlas.cz/dezinformace/) médií a zanalyzovali posledních 250 příspěvků každé z nich. Pomocí rozhraní Facebooku jsme hledali uživatele, kteří „lajkují“ příspěvky z více stránek a tvoří tak jejich spojnice. Celkem jsme zjistili na 120 tisíc vzájemných propojení.

<aside class="small"><br />&nbsp;<br />
<i>Na mapě jsou zobrazena všechna spojení s více než 60 společnými „lajkujícími“. Čím vzdálenější jsou od sebe jednotlivá média, tím méně společných spojení mají. Přesný počet spojení je uveden v tabulce níže.</i>
</aside>
<aside class="big">
  <div id="net" style="height: 800px; max-height: 95vh;">
</aside>

Nejsilnější spojnice jsou mezi médii, která lze označit za obecně zpravodajská. Patří mezi ně iDnes.cz, ČT24, ale také týdeník Reflex. 

Speciální podkategorii pak tvoří média z vydavatelského domu Economia: Aktuálně.cz, Hospodářské noviny, týdeník Respekt a také internetová televize DVTV, která s Economií spolupracuje. Vzájemné vazby mezi nimi jsou silnější než s obecně zpravodajskou konkurencí.

Zvláštní skupinou jsou nové projekty, které založili odpadlíci z tradičních redakcí: Týdeník Echo bývalého šéfredaktora Lidových novin Dalibora Balšínka a tři weby vedené exšéfy Mladé fronty Dnes: Svobodné fórum Pavla Šafra, Neovlivní Sabiny Slonkové a Reportér Roberta Čásenského. Jejich čtenáři dávají těmto třem webům a jednomu časopisu přednost před většinou obecně zpravodajských médií.

<aside class="big">
  <div id="matrix">
</aside>

Výraznou zvláštní dvojici tvoří také dvě webová média, která se označují za levicová - A2larm a Deník Referendum. Vazba mezi nimi je ze sledovaných médií vůbec nejsilnější a konkurence silně zaostává; další, o poznání slabší propojení mají s týdeníkem Respekt.

Vlastní „ostrov“ oddělený od ostatních médií tvoří čistě dezinformační weby - Aeronet, AC24 a Sputnik. Jediný výraznější most mezi nimi a obecně zpravodajskými médii tvoří web, který míchá pravdivé zprávy a dezinformace - Parlamentní listy.